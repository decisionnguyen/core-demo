module.exports = {
  root: true,
  extends: [
    '@react-native-community/eslint-config',
    'eslint-config-prettier',
  ],
  "plugins": [
    "react-hooks"
  ],
  "parser": "@babel/eslint-parser",
  rules: {
    'prettier/prettier': 0,
    "react-hooks/rules-of-hooks": "off",
    "react/react-in-jsx-scope": "off",
    "react-hooks/exhaustive-deps": "off",
    "no-unused-vars": "off",
    "eslint-comments/no-unused-disable":"off",
    "react-native/no-inline-styles":"off",
    "no-return-assign": "off",
    "radix":"off",
    "semi": "off",
    "no-shadow": "off"
  },
};
