# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /usr/local/Cellar/android-sdk/24.3.3/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# Reanimated: If you're using Proguard, make sure to add rule preventing it from optimizing Turbomodule classes
-keep class com.swmansion.reanimated.** { *; }
-keep class com.facebook.react.turbomodule.** { *; }

# Hermes: Also, if you're using ProGuard, you will need to add these rules in proguard-rules.pro :
-keep class com.facebook.hermes.unicode.** { *; }
-keep class com.facebook.jni.** { *; }

# react-native-device-info: If you want to use Install Referrer tracking, you will need to add this config to your Proguard config
-keepclassmembers class com.android.installreferrer.api.** {
  *;
}
# If you are experiencing issues with hasGms() on your release apks, please add the following rule to your Proguard config
-keep class com.google.android.gms.common.** {*;}
