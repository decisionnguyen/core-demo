/*
 * Jira Ticket:
 * Zeplin Design:
 * Feature Document:
 * Created Date: Thu, 24th Feb 2022, 10:20:31 am
 * Author: Harry Crank (harry.crank@thedistance.co.uk)
 * Copyright (c) 2022 The Distance
 */

import gql from 'graphql-tag';

const Address = gql`
  type Address {
    formatted: String
    streetAddress: String
    city: String
    postalCode: String
    country: String
  }
`;

export const TypeDefs = [Address];
