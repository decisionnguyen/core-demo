/*
 * Jira Ticket:
 * Zeplin Design:
 * Feature Document:
 * Created Date: Fri, 14th Jan 2022, 10:17:35 am
 * Author: Harry Crank (harry.crank@thedistance.co.uk)
 * Copyright (c) 2022 The Distance
 */

import {
  HttpLink,
  InMemoryCache,
  ApolloClient as ApolloClientBase,
  ApolloLink,
} from '@apollo/client';
import {persistCache, MMKVWrapper} from 'apollo3-cache-persist';
import {onError} from '@apollo/client/link/error';
import {RetryLink} from '@apollo/client/link/retry';

import {Environment, fetchSecrets} from '../environment';
import {
  firebaseAnalyticsEvent,
  firebaseCrashlyticsEvent,
  STORAGE,
} from '../utils';
import {TypeDefs} from './type-defs';
import {Authoriser} from './authoriser';
import {GlobalStorage, ApolloCacheStorage} from '../utils/storage-utils';

export const ApolloClient = async () => {
  // Set up the fetch and headers
  const apolloFetch = async (uri, options) => {
    const storedLocale = GlobalStorage.getString(STORAGE.LANGUAGE);

    const locale = storedLocale || 'en-GB';
    const translateMap = {
      'en-GB': 'en',
    };

    // User Apollo Middleware
    const Authorization = await Authoriser();
    const updatedOptions = {
      ...options,
      headers: {
        ...options.headers,
        Authorization,
        'Accept-Language': translateMap[locale],
      },
    };

    return fetch(uri, updatedOptions);
  };

  // Set up Link to external GraphQL endpoint
  const secrets = fetchSecrets(Environment);
  const graphQLUrl = secrets.graphQLUrl ?? '';

  const httpLink = new HttpLink({
    uri: graphQLUrl,
    fetch: apolloFetch,
  });

  const retryLink = new RetryLink({
    delay: {
      initial: 300,
      max: Infinity,
      jitter: true,
    },
    attempts: {
      max: 3,
      retryIf: (error, _operation) => !!error,
    },
  });

  const errorLink = onError(error => {
    const {graphQLErrors, networkError} = error;

    if (graphQLErrors) {
      graphQLErrors.map(({message, locations, path, extensions}) => {
        console.log(
          `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`,
        );
        firebaseAnalyticsEvent('GRAPHQL_ERROR', {
          message: `${message}`,
          code: `${extensions?.code}`,
          location: `${JSON.stringify(locations)}`,
          path: `${path}`,
        });
      });
    }

    if (networkError) {
      console.log(`[Network error]: ${networkError}`, networkError);
      firebaseAnalyticsEvent('NETWORK_ERROR', {
        message: `${networkError}`,
      });
    }

    firebaseCrashlyticsEvent();
  });

  const cache = new InMemoryCache({});

  await persistCache({
    cache,
    storage: new MMKVWrapper(ApolloCacheStorage),
  });

  const client = new ApolloClientBase({
    link: ApolloLink.from([errorLink, retryLink, httpLink]),
    cache: cache,
    typeDefs: TypeDefs,
    connectToDevTools: true,
  });

  return client;
};
