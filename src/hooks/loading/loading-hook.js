/*
 * Jira Ticket:
 * Zeplin Design:
 * Feature Document:
 * Created Date: Fri, 14th Jan 2022, 10:17:35 am
 * Author: Harry Crank (harry.crank@thedistance.co.uk)
 * Copyright (c) 2022 The Distance
 */

import React, {useMemo, createContext, useContext, useState} from 'react';

// ** ** ** ** ** CREATE ** ** ** ** **
const LoadingContext = createContext(null);

// ** ** ** ** ** USE ** ** ** ** **
export function useLoading() {
  const context = useContext(LoadingContext);
  if (context === undefined || context === null) {
    throw new Error(
      '`useLoading` hook must be used within a `LoadingProvider` component',
    );
  }
  return context;
}

// ** ** ** ** ** PROVIDE ** ** ** ** **
export function LoadingProvider({children}) {
  // ** ** ** ** ** SETUP ** ** ** ** **
  const [loading, setLoading] = useState(false);

  // ** ** ** ** ** MEMOIZE ** ** ** ** **
  const values = useMemo(() => ({loading, setLoading}), [loading, setLoading]);

  // ** ** ** ** ** RENDER ** ** ** ** **
  return (
    <LoadingContext.Provider value={values}>{children}</LoadingContext.Provider>
  );
}
