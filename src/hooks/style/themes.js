import {palette} from './palette';

const light = {
  backgroundColor: palette.white100,
};

const dark = {
  backgroundColor: palette.black100,
};

export {light, dark};
