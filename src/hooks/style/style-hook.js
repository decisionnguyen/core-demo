/*
 * Jira Ticket:
 * Zeplin Design:
 * Feature Document:
 * Created Date: Thu, 24th Feb 2022, 10:20:31 am
 * Author: Harry Crank (harry.crank@thedistance.co.uk)
 * Copyright (c) 2022 The Distance
 */

import React, {
  createContext,
  useContext,
  useEffect,
  useMemo,
  useState,
  useCallback,
} from 'react';
import {Dimensions, StatusBar, View, Appearance} from 'react-native';
import {useScale} from 'the-core-ui-utils';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {theme as DefaultElementsTheme} from 'the-core-ui-module-tdforms-v2';
import {ThemeProvider as ElementsThemeProvider} from 'react-native-elements';

import {palette} from './palette';
import {typography} from './typography';
import {dark, light} from './themes';
import {constants, STORAGE, THEME_MODES} from '../../utils/constants';
import {GlobalStorage} from '../../utils/storage-utils';
// ** ** ** ** ** CREATE ** ** ** ** **
const StyleContext = createContext(null);

// ** ** ** ** ** USE ** ** ** ** **
export function useStyle() {
  const context = useContext(StyleContext);
  if (context === undefined || context === null) {
    throw new Error(
      '`useStyle` hook must be used within a `StyleProvider` component',
    );
  }
  return context;
}

function getHeightRatio(layout) {
  const ratio = layout.height / layout.width;
  const heightRatio = Math.round(ratio * 9 * 10) / 10;
  return heightRatio;
}

const INITIAL_DIMENSIONS = Dimensions.get('window');

const DEFAULT_DIMENSIONS_CONTEXT_VALUE = {
  width: INITIAL_DIMENSIONS.width,
  height: INITIAL_DIMENSIONS.height,
  heightRatio: getHeightRatio(INITIAL_DIMENSIONS),
};

// ** ** ** ** ** PROVIDE ** ** ** ** **
export function StyleProvider({children}) {
  // ** ** ** ** ** SETUP ** ** ** ** **
  const {getHeight, getWidth, getFontSize, getRadius} = useScale();
  const insets = useSafeAreaInsets();
  const deviceSettingColorMode = Appearance.getColorScheme();

  const [layout, setLayout] = useState(DEFAULT_DIMENSIONS_CONTEXT_VALUE);
  const handleLayout = ({nativeEvent}) => {
    const {width, height} = nativeEvent.layout;
    setLayout({width, height, heightRatio: getHeightRatio(nativeEvent.layout)});
  };

  const [colorMode, setColorMode] = useState(THEME_MODES.LIGHT);

  const [mainStatusBar, setMainStatusBar] = useState(
    colorMode === THEME_MODES.LIGHT
      ? THEME_MODES.DARK_CONTENT
      : THEME_MODES.LIGHT_CONTENT,
  );

  const [coloursTheme, setColoursTheme] = useState(light);

  const [elementsTheme, setElementsTheme] = useState(DefaultElementsTheme);
  useEffect(() => {
    setElementsTheme({
      ...elementsTheme,
      Icon: {
        solid: true,
        color: 'blue',
        size: 16,
      },
      ExternalIcon: {
        solid: true,
        color: 'blue',
        size: 24,
      },
    });
  }, []);

  // Fetch the saved color mode from Storage, if it exists set the color mode to
  // the saved value, otherwise set it to the default device setting color mode
  const fetchSavedColorMode = useCallback(async () => {
    try {
      const savedColorMode = GlobalStorage.getString(STORAGE.COLOUR_MODE);
      console.log('StyleProvider - savedColorMode: ', savedColorMode);
      if (savedColorMode) {
        setColorMode(savedColorMode);
      } else {
        setColorMode(deviceSettingColorMode);
      }
    } catch (e) {
      console.log('StyleProvider - fetchSavedColorMode - error: ', e);
      setColorMode(deviceSettingColorMode);
    }
  }, [deviceSettingColorMode]);

  // Initial theme load
  useEffect(() => {
    fetchSavedColorMode();
  }, [deviceSettingColorMode, fetchSavedColorMode]);

  // Handle StatusBar theme change events
  useEffect(() => {
    StatusBar.setBarStyle(mainStatusBar);
  }, [mainStatusBar]);

  useEffect(() => {
    console.log('colorMode:set:', colorMode);
    // todo: need to change the react native elements theme as well to apply onto FormsV2
    setColoursTheme(colorMode === THEME_MODES.DARK ? dark : light);
    setMainStatusBar(
      colorMode === THEME_MODES.LIGHT
        ? THEME_MODES.DARK_CONTENT
        : THEME_MODES.LIGHT_CONTENT,
    );
  }, [colorMode]);

  // ** ** ** ** ** METHODS ** ** ** ** **
  const dimensionConstants = useMemo(() => {
    return {
      HEADER_HEIGHT: 49 + insets.top,
      SCREEN_LAYOUT: layout,
    };
  }, [insets.top, layout]);

  const textStyles = useMemo(() => {
    return {
      textStyle1: {
        fontFamily: typography.bold,
        color: palette.darkBlue100,
        fontSize: getFontSize(24),
        textAlign: 'left',
      },
    };
  }, [getFontSize]);

  const spacing = useMemo(() => {
    return {
      small: getFontSize(10),
      medium: getFontSize(20),
      large: getFontSize(40),
      xLarge: getFontSize(80),
    };
  }, [getFontSize]);

  // ** ** ** ** ** MEMOIZE ** ** ** ** **
  const values = useMemo(
    () => ({
      dimensionConstants,
      textStyles,
      spacing,
      palette,
      typography,
      coloursTheme,
    }),
    [dimensionConstants, textStyles, spacing, coloursTheme],
  );

  // ** ** ** ** ** RENDER ** ** ** ** **
  return (
    <ElementsThemeProvider theme={elementsTheme}>
      <View onLayout={handleLayout} style={{flex: 1}}>
        <StyleContext.Provider value={values}>{children}</StyleContext.Provider>
      </View>
    </ElementsThemeProvider>
  );
}
