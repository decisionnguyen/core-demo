/*
 * Jira Ticket:
 * Zeplin Design:
 * Feature Document:
 * Created Date: Fri, 14th Jan 2022, 10:17:35 am
 * Author: Harry Crank (harry.crank@thedistance.co.uk)
 * Copyright (c) 2022 The Distance
 */

export const palette = {
  apple100: 'rgba(145,197,61,1)',
  green100: 'rgba(97,185,71,1)',
  lightBlue28: 'rgba(1,103,151,0.28)',
  lightBlue100: 'rgba(1,103,151,1)',
  darkBlue0: 'rgba(14,75,100,0)',
  darkBlue70: 'rgba(14,75,100,0.7)',
  darkBlue100: 'rgba(14,75,100,1)',
  reddish100: 'rgba(197,61,61,1)',
  lightGrey100: 'rgba(244,244,244,1)',
  white0: 'rgba(255,255,255,0)',
  white30: 'rgba(255,255,255,0.3)',
  white60: 'rgba(255,255,255,0.6)',
  white100: 'rgba(255,255,255,1)',
  darkGrey100: 'rgba(168,168,168,1)',
  black100: 'rgba(0,0,0,1)',
  transparent0: 'rgba(0,0,0,0)',
};
