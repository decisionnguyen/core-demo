/*
 * Jira Ticket:
 * Zeplin Design:
 * Feature Document:
 * Created Date: Fri, 14th Jan 2022, 10:17:35 am
 * Author: Harry Crank (harry.crank@thedistance.co.uk)
 * Copyright (c) 2022 The Distance
 */

import {Platform} from 'react-native';

export const typography = {
  light: Platform.select({ios: 'Helvetica', android: 'normal'}),
  bold: Platform.select({ios: 'Arial', android: 'sans-serif'}),
};
