/*
 * Jira Ticket:
 * Zeplin Design:
 * Feature Document:
 * Created Date: Fri, 14th Jan 2022, 10:17:35 am
 * Author: Harry Crank (harry.crank@thedistance.co.uk)
 * Copyright (c) 2022 The Distance
 */

import React, {
  useCallback,
  useEffect,
  useMemo,
  useState,
  createContext,
  useContext,
} from 'react';
import {STORAGE} from '../../utils';
import {GlobalStorage} from '../../utils/storage-utils';

import {enGB} from './languages';

// ** ** ** ** ** CREATE ** ** ** ** **
const DictionaryContext = createContext(null);

// ** ** ** ** ** USE ** ** ** ** **
export function useDictionary() {
  const context = useContext(DictionaryContext);
  if (context === undefined || context === null) {
    throw new Error(
      '`useDictionary` hook must be used within a `DictionaryProvider` component',
    );
  }
  return context;
}

// ** ** ** ** ** PROVIDE ** ** ** ** **
export function DictionaryProvider({presetLocale, children}) {
  // ** ** ** ** ** SETUP ** ** ** ** **
  const [locale, setLocale] = useState(presetLocale);

  const languageDictionaryMap = useMemo(() => {
    return {'en-GB': enGB};
  }, []);

  const [dictionary, setDictionary] = useState(languageDictionaryMap[locale]);

  useEffect(() => {
    setDictionary(languageDictionaryMap[locale]);
  }, [locale, languageDictionaryMap]);

  const getLanguage = useCallback(() => {
    const language = GlobalStorage.getString(STORAGE.LANGUAGE);
    setLocale(language || 'en-GB');
  }, []);

  const saveLanguage = useCallback((language = 'en-GB') => {
    GlobalStorage.set(STORAGE.LANGUAGE, language);
  }, []);

  const setLanguage = useCallback(
    async value => {
      // Update session locale
      setLocale(value);
      // Update saved value
      return await saveLanguage(value);
    },
    [saveLanguage],
  );

  // ** ** ** ** ** MEMOIZE ** ** ** ** **
  const values = React.useMemo(
    () => ({dictionary, getLanguage, setLanguage}),
    [dictionary, getLanguage, setLanguage],
  );

  // ** ** ** ** ** RENDER ** ** ** ** **
  return (
    <DictionaryContext.Provider value={values}>
      {children}
    </DictionaryContext.Provider>
  );
}
