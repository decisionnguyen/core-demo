/*eslint sort-keys: ["warn", "asc", {"caseSensitive": false, "natural": true}]*/
/*
 * Jira Ticket:
 * Zeplin Design:
 * Feature Document:
 * Created Date: Fri, 14th Jan 2022, 10:17:35 am
 * Author: Harry Crank (harry.crank@thedistance.co.uk)
 * Copyright (c) 2022 The Distance
 */

export const enGB = {
  InitialBuildScreen: {
    Description: 'Default Template Screen',
  },
};
