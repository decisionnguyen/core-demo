import {useNetInfo} from '@react-native-community/netinfo';
import React, {useState, useEffect} from 'react';

const Context = React.createContext(null);

export function useNetwork() {
  const context = React.useContext(Context);

  if (context === undefined) {
    throw new Error(
      '`useNetwork` hook must be used within a `NetworkProvider` component',
    );
  }
  return context;
}

export function NetworkProvider(props) {
  const {isConnected} = useNetInfo();
  const [isConnectedToNetwork, setIsConectedToNetwork] = useState(true);

  useEffect(() => {
    setIsConectedToNetwork(isConnected);
  }, [isConnected]);

  // ** ** ** ** ** Memoize ** ** ** ** **
  const values = React.useMemo(
    () => ({
      isConnected,
      isConnectedToNetwork,
    }),
    [isConnected, isConnectedToNetwork],
  );

  // ** ** ** ** ** Return ** ** ** ** **
  return <Context.Provider value={values}>{props.children}</Context.Provider>;
}
