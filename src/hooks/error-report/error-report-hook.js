/*
 * Jira Ticket:
 * Zeplin Design:
 * Feature Document:
 * Created Date: Thu, 24th Feb 2022, 10:20:31 am
 * Author: Harry Crank (harry.crank@thedistance.co.uk)
 * Copyright (c) 2022 The Distance
 */

import {STORAGE, firebaseCrashlyticsEvent} from '../../utils';
import React, {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useMemo,
} from 'react';
import {
  setJSExceptionHandler,
  setNativeExceptionHandler,
} from 'react-native-exception-handler';

import ErrorBoundary from 'react-native-error-boundary';
// import analytics from '@react-native-firebase/analytics';
// import crashlytics from '@react-native-firebase/crashlytics';
import {GlobalStorage} from '../../utils/storage-utils';

// ** ** ** ** ** CREATE ** ** ** ** **
const ErrorReportContext = createContext(null);

// ** ** ** ** ** USE ** ** ** ** **
export function useErrorReport() {
  const context = useContext(ErrorReportContext);
  if (context === undefined || context === null) {
    throw new Error(
      '`useErrorReport` hook must be used within a `ErrorReportProvider` component',
    );
  }
  return context;
}

const errorHandler = (e, isFatal) => {
  console.log('setJSExceptionHandler:', e, isFatal);
  if (isFatal) {
  } else {
  }
  firebaseCrashlyticsEvent(e);
};

setJSExceptionHandler(errorHandler, true);

setNativeExceptionHandler(errorString => {
  console.log('setNativeExceptionHandler:', errorString);
  firebaseCrashlyticsEvent(errorString);
});

// ** ** ** ** ** PROVIDE ** ** ** ** **
export function ErrorReportProvider(props) {
  // ** ** ** ** ** SETUP ** ** ** ** **
  useEffect(() => {
    // analytics()
    //   .getAppInstanceId()
    //   .then(it => console.log('FirebaseAnalytics.AppInstanceId:', it))
    //   .catch(er => console.log('FirebaseAnalytics.AppInstanceId:', er));

    // crashlytics().setCrashlyticsCollectionEnabled(true);
    // analytics().setAnalyticsCollectionEnabled(true);
    // init();
  }, [init]);

  const init = useCallback(async () => {
    const analyticsAllowed = GlobalStorage.getBoolean(STORAGE.ANALYTICS);
    const crashlyticsAllowed = GlobalStorage.getBoolean(STORAGE.CRASHLYTICS);

    // analytics().setAnalyticsCollectionEnabled(!!analyticsAllowed);
    // crashlytics().setCrashlyticsCollectionEnabled(!!crashlyticsAllowed);
  }, []);

  // ** ** ** ** ** MEMOIZE ** ** ** ** **
  const values = useMemo(() => ({init}), [init]);

  // ** ** ** ** ** RENDER ** ** ** ** **
  return (
    <ErrorReportContext.Provider value={values}>
      <ErrorBoundary
        onError={(error, trace) => firebaseCrashlyticsEvent(error)}>
        {props.children}
      </ErrorBoundary>
    </ErrorReportContext.Provider>
  );
}
