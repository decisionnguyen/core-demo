/*
 * Jira Ticket:
 * Zeplin Design:
 * Feature Document:
 * Created Date: Fri, 14th Jan 2022, 10:17:35 am
 * Author: Harry Crank (harry.crank@thedistance.co.uk)
 * Copyright (c) 2022 The Distance
 */

import {NativeModules, Platform} from 'react-native';

import {Environment} from './environment';

const SecretsManager =
  Platform.OS === 'ios'
    ? NativeModules.iOSSecretsManager
    : NativeModules.AndroidSecretsManager;

export function fetchSecrets() {
  try {
    const secrets = SecretsManager.fetch(Environment);
    return secrets;
  } catch (error) {
    console.error(error);
    return null;
  }
}
