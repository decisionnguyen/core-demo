/* eslint-disable react-native/no-inline-styles */
/*
 * Created Date: Fri, 27th Dec 2019, 21:02:13 pm
 * Author: James Shaw
 * Email: james.shaw@thedistance.co.uk
 * Copyright (c) 2019 The Distance
 */

import React, {useEffect} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import VersionInfo from 'react-native-version-info';

import {fetchSecrets} from '../../environment';
import {useLoading, useStyle} from '../../hooks';
import {GlobalStorage} from '../../utils/storage-utils';
// import {useDictionary} from '../../hooks';

// const image = require('../../assets/images/initial-release/initial-release.png');

export function InitialReleaseScreen() {
  const {setLoading} = useLoading();
  const {dimensionConstants, palette, spacing, coloursTheme} = useStyle();
  // const {dictionary} = useDictionary();
  // const {InitialBuildScreen} = dictionary;

  useEffect(() => {
    setLoading(true);

    setTimeout(() => {
      setLoading(false);
    }, 3000);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    SplashScreen.hide();

    GlobalStorage.set('user.age', 12344);
    GlobalStorage.set('user.name', 'john');

    const secrets = fetchSecrets();
    console.log('Fetched secrets:', secrets);
  }, []);

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: coloursTheme.backgroundColor,
    },
    image: {
      width: dimensionConstants.SCREEN_LAYOUT.width,
    },
  });
  return (
    <View style={styles.container}>
      {__DEV__ && (
        <Text style={{position: 'absolute', bottom: 30, right: 30}}>
          ok o ko kk
          {VersionInfo.appVersion} - Hermes Enabled:{' '}
          {(typeof global.HermesInternal !== 'undefined').toString()}
        </Text>
      )}
    </View>
  );
}
