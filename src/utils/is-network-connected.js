/*
 * Jira Ticket:
 * Zeplin Design:
 * Feature Document:
 * Created Date: Thu, 24th Feb 2022, 10:20:31 am
 * Author: Harry Crank (harry.crank@thedistance.co.uk)
 * Copyright (c) 2022 The Distance
 */

import {NetInfo} from 'react-native';
import * as R from 'ramda';

export async function isNetworkConnected() {
  const connected = await NetInfo.getConnectionInfo()
    .then(connectionInfo => {
      const type = R.path(['type'], connectionInfo);
      if (type === 'unknown' || type === 'none') {
        return false;
      }
      return true;
    })
    .catch(err => {
      console.log('Network Connection Error:', err);
      return false;
    });

  return connected;
}
