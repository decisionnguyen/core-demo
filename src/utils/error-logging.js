/*
 * Jira Ticket:
 * Zeplin Design:
 * Feature Document:
 * Created Date: Thu, 24th Feb 2022, 13:07:16 pm
 * Author: Harry Crank (harry.crank@thedistance.co.uk)
 * Copyright (c) 2022 The Distance
 */
//
// import analytics from '@react-native-firebase/analytics';
// import crashlytics from '@react-native-firebase/crashlytics';
import {format} from 'date-fns';

import {STORAGE} from './constants';
import {GlobalStorage} from './storage-utils';

export const firebaseAnalyticsEvent = async (event, params = {}) => {
  const allowed = GlobalStorage.getBoolean(STORAGE.ANALYTICS);

  if (JSON.parse(allowed)) {
    const time = format(new Date(), 'hh:mm');
    const date = format(new Date(), 'DD/MM/YYYY');
    console.log('firebaseAnalyticsEvent', event + ' at ' + time + ', ' + date);

    // analytics().logEvent(event, params);
  } else {
    console.log(STORAGE.ANALYTICS, !!allowed);
  }
};

export const firebaseCrashlyticsEvent = async error => {
  const allowed = GlobalStorage.getBoolean(STORAGE.CRASHLYTICS);

  if (JSON.parse(allowed)) {
    if (typeof error === 'string') {
      // crashlytics().recordError(new Error(error));
    } else if (error) {
      // crashlytics().recordError(error);
    } else {
      // crashlytics().recordError(new Error());
    }
  } else {
    console.log(STORAGE.CRASHLYTICS, !!allowed);
  }
};
