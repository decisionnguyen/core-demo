/*
 * Jira Ticket:
 * Zeplin Design:
 * Feature Document:
 * Created Date: Fri, 14th Jan 2022, 10:17:35 am
 * Author: Harry Crank (harry.crank@thedistance.co.uk)
 * Copyright (c) 2022 The Distance
 */

const hexToRgb = hex => {
  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result
    ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16),
      }
    : null;
};

const applyOpacity = (inputHex, alpha) => {
  const rgb = hexToRgb(inputHex);

  if (rgb) {
    return `rgba(${rgb.r}, ${rgb.g}, ${rgb.b}, ${alpha})`;
  }

  return inputHex;
};

export {hexToRgb, applyOpacity};
