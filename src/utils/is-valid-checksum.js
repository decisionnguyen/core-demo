/*
 * Jira Ticket:
 * Zeplin Design:
 * Feature Document:
 * Created Date: Thu, 24th Feb 2022, 10:20:31 am
 * Author: Harry Crank (harry.crank@thedistance.co.uk)
 * Copyright (c) 2022 The Distance
 */

import RNBundleChecksum from 'react-native-bundle-checksum';

import {Environment, fetchSecrets} from '../environment';

export async function isValidChecksum() {
  if (!__DEV__) {
    try {
      const checksum = await RNBundleChecksum.getChecksum();
      const correctChecksum = fetchSecrets(Environment).checksum;
      const checksumTampered = checksum !== correctChecksum;
      return !checksumTampered;
    } catch (e) {
      console.error(e);
      return false;
    }
  }

  return true;
}
