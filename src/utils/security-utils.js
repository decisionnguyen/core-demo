import JailMonkey from 'jail-monkey';
import {isEmulatorSync} from 'react-native-device-info';

const RESTRICTIONS = {
  JAIL_BROKEN: 'JAIL_BROKEN',
  MOCK_LOCATION: 'MOCK_LOCATION',
  SUSPICIOUS_APP: 'SUSPICIOUS_APP',
  DEBUG_BRIDGE_ATTACHED: 'DEBUG_BRIDGE_ATTACHED',
  IS_EMULATOR: 'IS_EMULATOR',
};

const restrictAccess = (req, res) => {
  if (__DEV__) {
    return false;
  }

  if (JailMonkey.isJailBroken) {
    return RESTRICTIONS.JAIL_BROKEN;
  } else if (JailMonkey.canMockLocation) {
    return RESTRICTIONS.MOCK_LOCATION;
  } else if (JailMonkey.hookDetected) {
    return RESTRICTIONS.SUSPICIOUS_APP;
  } else if (JailMonkey.AdbEnabled) {
    return RESTRICTIONS.DEBUG_BRIDGE_ATTACHED;
  } else if (isEmulatorSync()) {
    return RESTRICTIONS.IS_EMULATOR;
  }
};

export {restrictAccess};
