/*eslint sort-keys: ["warn", "asc", {"caseSensitive": false, "natural": true}]*/
/*
 * Jira Ticket:
 * Zeplin Design:
 * Feature Document:
 * Created Date: Thu, 24th Feb 2022, 10:39:31 am
 * Author: Harry Crank (harry.crank@thedistance.co.uk)
 * Copyright (c) 2022 The Distance
 */

// For Storage getters/setters.
export const STORAGE = {
  ANALYTICS: '@analytics_allowed',
  COLOUR_MODE: '@colorMode',
  CRASHLYTICS: '@crashlytics_allowed',
  LANGUAGE: '@language',
};

// For use throughout the app where values are constant.
export const constants = {
  ANDROID: 'android',
  IOS: 'ios',
};

export const THEME_MODES = {
  DARK: 'dark',
  DARK_CONTENT: 'dark-content',
  LIGHT: 'light',
  LIGHT_CONTENT: 'light-content',
};
