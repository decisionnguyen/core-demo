import I18n from 'react-native-i18n';
/**
 *
 * @param {*} unformattedDate
 * @returns formattedShortDate
 */
export const formatShortDate = unformattedDate => {
  var locale = I18n.currentLocale();

  const date = new Date(unformattedDate);
  return new Intl.DateTimeFormat(locale, {
    month: '2-digit',
    day: '2-digit',
    year: '2-digit',
  }).format(date);
};

/**
 *
 * @param {*} unformattedDate
 * @returns formattedLongDate
 */
export const formatLongDate = unformattedDate => {
  var locale = I18n.currentLocale();

  const date = new Date(unformattedDate);
  return new Intl.DateTimeFormat(locale, {
    weekday: 'short',
    year: 'numeric',
    month: 'long',
    day: '2-digit',
  }).format(date);
};

/**
 *
 * @param {*} unformattedTime
 * @returns formattedTime
 */
export const formatTime = unformattedTime => {
  var locale = I18n.currentLocale();
  const time = unformattedTime.split(':');
  const date = new Date().setHours(parseInt(time[0]), parseInt(time[1]));
  let instance = new Intl.DateTimeFormat(locale, {
    hour: '2-digit',
    minute: '2-digit',
  })
  let formattedTime = instance.format(date);

  return formattedTime;
};
/**
 *
 * @param {*} unformattedNumber
 * @returns formattedNumber
 */
export const formatNumber = unformattedNumber => {
  var locale = I18n.currentLocale();
  return new Intl.NumberFormat(locale).format(unformattedNumber);
};
