import { useRef } from 'react'

/**
 * use to make sure double/multiple taps should not call click event multiple times
 * @param {*} callback function to be called when clicked on button
 * @param {*} timeBlocked default is 400 
 * @returns 
 */
export default function withDebouce(callback, timeBlocked = 400) {
  const isBlockedRef = useRef(false)
  const unblockTimeout = useRef(false)

  return (...callbackArgs) => {
    if (!isBlockedRef.current) {
      callback(...callbackArgs)
    }
    clearTimeout(unblockTimeout.current)
    unblockTimeout.current = setTimeout(() => isBlockedRef.current = false, timeBlocked)
    isBlockedRef.current = true
  }
}
