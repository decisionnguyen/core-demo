/*
 * Jira Ticket:
 * Zeplin Design:
 * Feature Document:
 * Created Date: Fri, 14th Jan 2022, 10:17:35 am
 * Author: Harry Crank (harry.crank@thedistance.co.uk)
 * Copyright (c) 2022 The Distance
 */

import React, {Fragment} from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import {InitialReleaseScreen} from '../screens';
import {useLoading} from '../hooks';
import {LoadingView} from '../components';

export const AppStack = createStackNavigator();

export function AppContainer() {
  const {loading} = useLoading();
  return (
    <Fragment>
      <AppStack.Navigator>
        <AppStack.Screen
          name="Initial Release"
          component={InitialReleaseScreen}
          options={{
            headerShown: false,
          }}
        />
      </AppStack.Navigator>
      {loading && LoadingView()}
    </Fragment>
  );
}
