/* eslint-disable react-native/no-inline-styles */
/*
 * Jira Ticket:
 * Zeplin Design:
 * Feature Document:
 * Created Date: Thu, 24th Feb 2022, 10:20:31 am
 * Author: Harry Crank (harry.crank@thedistance.co.uk)
 * Copyright (c) 2022 The Distance
 */

import React, {Fragment, useCallback, useEffect, useState} from 'react';
import {I18nManager, StatusBar, View, Text, TextInput} from 'react-native';
import {ApolloProvider} from '@apollo/client';
import {NavigationContainer} from '@react-navigation/native';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {ScaleProvider} from 'the-core-ui-utils';
import SplashScreen from 'react-native-splash-screen';

import {ApolloClient} from './apollo';
import {
  DictionaryProvider,
  ErrorReportProvider,
  LoadingProvider,
  StyleProvider,
} from './hooks';
import {AppContainer} from './navigation';
import {isValidChecksum, restrictAccess, STORAGE} from './utils';
import {GlobalStorage} from './utils/storage-utils';
import {NetworkProvider} from './hooks/network/network-hook';

Text.defaultProps = Text.defaultProps || {};
Text.defaultProps.allowFontScaling = false;
TextInput.defaultProps = TextInput.defaultProps || {};
TextInput.defaultProps.allowFontScaling = false;

export function App() {
  const [client, setClient] = useState(null);
  const [validChecksum, setValidChecksum] = useState(true);
  const [presetLocale, setPresetLocale] = useState();

  const setupApollo = async () => {
    const newClient = await ApolloClient();
    setClient(newClient);
  };

  const validateChecksum = async () => {
    const valid = await isValidChecksum();
    setValidChecksum(valid);
  };

  useEffect(() => {
    SplashScreen.hide();
  });

  useEffect(() => {
    if (!client) {
      setupApollo();
    }
  }, [client]);

  useEffect(() => {
    validateChecksum();
  }, []);

  useEffect(() => {
    const language = GlobalStorage.getString(STORAGE.LANGUAGE);

    // Note: Handle reset language
    if (I18nManager.isRTL) {
      I18nManager.forceRTL(false);
    }

    setPresetLocale(language || 'en-GB');
  }, []);

  if (restrictAccess()) {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: 'white',
          alignItems: 'center',
          justifyContent: 'center',
          flexDirection: 'column',
        }}>
        <Text
          style={{
            fontSize: 30,
            fontWeight: 'bold',
            textAlign: 'center',
          }}>
          Oops!
        </Text>
        <Text
          style={{
            fontSize: 18,
            textAlign: 'center',
            marginHorizontal: 40,
            marginTop: 10,
          }}>
          You are not allowed to access this application
        </Text>
      </View>
    );
  }

  if (!validChecksum) {
    return <View style={{flex: 1, backgroundColor: 'red'}} />;
  }

  if (!client) {
    return <View style={{flex: 1, backgroundColor: 'pink'}} />;
  }

  if (!presetLocale) {
    return <View style={{flex: 1, backgroundColor: 'teal'}} />;
  }

  return (
    <NetworkProvider>
      <ErrorReportProvider>
        <ApolloProvider client={client}>
          <SafeAreaProvider>
            <ScaleProvider config={{height: 812, width: 375}}>
              <StyleProvider>
                {/* Pass preset locale value on provider to avoid rendering wrong default value   */}
                <DictionaryProvider presetLocale={presetLocale}>
                  <LoadingProvider>
                    <NavigationContainer>
                      <AppContainer />
                    </NavigationContainer>
                  </LoadingProvider>
                </DictionaryProvider>
              </StyleProvider>
            </ScaleProvider>
          </SafeAreaProvider>
        </ApolloProvider>
      </ErrorReportProvider>
    </NetworkProvider>
  );
}
