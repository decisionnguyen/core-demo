# React Native Project Setup

## Requirements before starting

1. Followed the [README](./README.md).
2. Have the necessary platform-specific app bundle identifiers.
3. Design Team have provided the iOS App Icons (20pt, 29pt, 40pt, 60pt & 1024pt).
4. App designs are available on Zeplin - (Styles & Fonts).
5. GraphQL schema has been created.
6. Platform specific:

- iOS - Following feature documents, update the General tab with the required Device (iPhone / iPad) and the Device Orientation (default to Portrait).

---

### App Identifier

Update the App Identifiers throughout the app.

#### iOS

NOTE: BundleId for iOS is automatically registered on the connected Apple Developer account. Make sure XCode points to the clients' Developer Account before setting this.

1. Open project in Xcode.
2. In the General view, update the `Bundle Identifier` with the correct value.

#### Android

1. In Visual Code Studio, update the `applicationId` in [android/app/build.gradle](./android/app/build.gradle), the `package` in [android/app/src/main/AndroidManifest.xml](./android/app/src/main/AndroidManifest.xml) and the folder structure in `./android/app/src/main/java` so a new directory is made for each part of the identifier (currently it is: com/thecoreui).

---

### App Icons

#### iOS

1. Open project in Xcode.
2. Navigate to the `Images.xcassets` folder within the main project folder (TheCoreUI).
3. Click on AppIcon, drag & drop the correct icon to match the required size.

#### Android

1. Open project in Android Studio.
2. Follow the steps in the [link](https://developer.android.com/studio/write/image-asset-studio#access) to add the icons.

---

### Splash Screen

#### iOS

1. Open project in Xcode.
2. Navigate to `LaunchScreen.xib` in the main project folder (TheCoreUI).
3. Update the screen with the new splash screen.

#### Android

1. Follow the steps in the [link](https://github.com/crazycodeboy/react-native-splash-screen#getting-started) to add the SplashScreen.

---

### Apple Developer account - Devices & UDIDs

#### iOS

The [UDID spreadsheet](https://docs.google.com/spreadsheets/d/18UO1m1WGcT56ZGNw1bHCMLc61KLatmqMLRA3p15bujE/edit#gid=0) can be used to populate the devices list on the project’s Apple Developer account.

1. Check the list and update for with new devices and/or remove devices no longer required (i.e. when people leave)
2. Ensure all 3 columns are completed (Device ID, Device Name, Device Platform).
3. Then click File -> Download -> Tab-separated values.
4. Locate the download and rename the file to end in `.txt` , accept the Finder warning to use `.txt`.
5. In the Apple developer account for the project, click on Devices.
6. Upload the txt file to the Register Multiple Devices.
7. Press Continue and verify that all devices are there.

#### Android

N/A

---

### App Center

To add [AppCenter](https://docs.microsoft.com/en-us/appcenter) and AppCenter crashes, please follow the guide [here](https://docs.microsoft.com/en-us/appcenter/sdk/getting-started/react-native).

1. Login in to [AppCenter](https://appcenter.ms/orgs/The-Distance/applications).
2. Click on `Add new app`.
3. Complete the App Name, OS (Android / iOS) and the Platform (React Native).
4. Click on `Build` and connect the repo through `BitBucket`. If it doesn't show, then the repo needs it's permissions amending to show on BitBucket.
5. A list of available committed branches will show. Click on the `spanner` on the right-hand side.
6. Configure the settings.
7. Complete setup by clicking 'Save and Build'.
8. Repeat the above steps (2-7) but for the other Platform.

---

### Firebase Crashlytics & Analytics

<p>
Firebase is already set up. After replacing the Application Id (Android) and the Bundle Id (iOS), create the project on Firebase console and replace the config files for each platform,  google-services.json (Android) and GoogleService-Info.plist (iOS).

---

### Import Zeplin Styles and Fonts

1. Open Zeplin and navigate through to `Styleguide`.
2. Locate [StyleProvider](./src/hooks/style/useStyle.js) in the style hook folder.
3. Add Colors, FontFamilies, FontSizes, FontWeights and TextStyles and use through the useStyle hook.
4. When adding colors, where possible, please use the rgba() color option as this makes it easier to update the alpha on it.
5. If planning on using react-native-elements components the Provider in App.js takes a Theme structure of what the theme for all elements components should follow.

https://reactnativeelements.com/docs/

```
import { ThemeProvider, Button } from 'react-native-elements';

const theme = {
  Button: {
    raised: true,
  },
};

// Your App
const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <Button title="My Button" />
      <Button title="My 2nd Button" />
    </ThemeProvider>
  );
};
```

---

### Setup GraphQL Schema and Apollo

1. Update the [TypeDefs](./src/apollo/) file.
2. Using the examples in the [README_APOLLO](./README_APOLLO.md). Update all the queries, mutations and implementations to follow the project schema.

---

### Setup Navigation Hierarchy

**WORK IN PROGRESS**

---

### Configure Environments

1. Update the Environment variables in the [env-vars.sh](./env-vars.sh) file created in Project setup.

---

### Distribute to AppCenter

1. Push the code to AppCenter to make sure it builds on both platforms and that they both open/work as expected i.e. show the Hooray image.
