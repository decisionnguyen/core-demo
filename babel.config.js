module.exports = {
    "presets": [
        ['@babel/preset-env', {targets: {node: 'current'}}],
        ['@babel/preset-react', {targets: {node: 'current'}}]
    ],
    "plugins": [
        'react-native-reanimated/plugin',
        "@babel/transform-runtime",
        [
            "@babel/plugin-proposal-decorators",
            {
                "legacy": true
            }
        ],
        "@babel/plugin-proposal-function-sent",
        "@babel/plugin-syntax-jsx",
        "@babel/plugin-proposal-export-namespace-from",
        "@babel/plugin-proposal-numeric-separator",
        "@babel/plugin-proposal-throw-expressions",
        "@babel/plugin-syntax-dynamic-import",
        "@babel/plugin-syntax-import-meta",
        ["@babel/plugin-proposal-class-properties", {"loose": true}],
        ["@babel/plugin-proposal-private-methods", {"loose": true}],
        ["@babel/plugin-proposal-private-property-in-object", {"loose": true}],
        "@babel/plugin-proposal-json-strings"
    ],
    "assumptions": {
        "privateFieldsAsProperties": true,
        "setPublicClassFields": true
    }
}
