const obfuscatingTransformer = require('react-native-obfuscating-transformer');

module.exports = obfuscatingTransformer({
  // this configuration is based on https://github.com/javascript-obfuscator/javascript-obfuscator
  obfuscatorOptions: {
    compact: true,
    controlFlowFlattening: false, // Control flow flattening is a structure transformation of the source code that hinders program comprehension
    controlFlowFlatteningThreshold: 0, // set percentage of nodes that will affected by control flow flattening
    deadCodeInjection: true, // random blocks of dead code will be added to the obfuscated code
    deadCodeInjectionThreshold: 0.1, // set percentage of nodes that will affected by dead code injection
    debugProtection: false, // This option makes it almost impossible to use the debugger function of the Developer Tools, freezes browser
    debugProtectionInterval: 0, //  an interval in milliseconds is used to force the debug mode on the Console tab 2000 - 4000 recomended
    disableConsoleOutput: true, // this option disables console calls globally for all scripts
    identifierNamesGenerator: 'hexadecimal',
    log: false,
    renameGlobals: false, // DO NOT CHANGE
    renameProperties: false, // DO NOT CHANGE
    selfDefending: true, // This option makes the output code resilient against formatting and variable renaming. If one tries to use a JavaScript beautifier on the obfuscated code, the code won't work anymore, making it harder to understand and modify it.
    simplify: true, // Enables additional code obfuscation through simplification.

    numbersToExpressions: false,
    splitStrings: false,
    stringArray: true, // Removes string literals and place them in a special array
    stringArrayEncoding: ['none', 'base64'], // some stringArray value won't be encoded, and some values will be encoded with base64
    stringArrayThreshold: 0.65,
    transformObjectKeys: false,
    unicodeEscapeSequence: false, // Unicode escape sequence increases code size greatly and strings easily can be reverted to their original view
  },
  upstreamTransformer: require('metro-react-native-babel-transformer'),
  emitObfuscatedFiles: false,
  enableInDevelopment: true,
  trace: true,
});
